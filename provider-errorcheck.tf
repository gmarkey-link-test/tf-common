terraform {

  required_providers {
    errorcheck = {
      source = "rhythmictech/errorcheck"
    }
  }

}

provider "errorcheck" {}
