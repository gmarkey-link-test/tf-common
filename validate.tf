resource "errorcheck_is_valid" "aws_account_matches_target" {

  name = "aws_account_must_match"
  test = {
    assert        = var.aws_account_id == var.target_aws_account_id
    error_message = "AWS account ID must match ${var.aws_account_id}; check AWS envvars"
  }

}


# This would be a better way of validating without requiring a third party provider.
# Requires https://github.com/hashicorp/terraform/pull/25088
#locals {
#  validate_account = var.aws_account_id == var.target_aws_account_id ? "" : raise ("AWS account ID must match ${var.aws_account_id}; check envvars")
#}
