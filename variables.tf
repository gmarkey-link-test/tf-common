variable "target_aws_account_id" {

  description = "AWS account ID that we are targeting for this Terraform execution."
  type        = string

}


variable "aws_account_id" {

  description = "This is the account ID that must be used for this project. Used to be super defensive and ensure that Terraform never tries to interact with other accounts due to user error."
  default     = "476298124253"
  type        = string

}
